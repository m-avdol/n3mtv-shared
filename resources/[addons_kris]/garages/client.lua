--
-- Created by IntelliJ IDEA.
-- User: Djyss
-- Date: 21/06/2017
-- Time: 14:27
-- To change this template use File | Settings | File Templates.

local locations = {
    {name= 'Garages', x = 215.124, y= -791.377, z= 29.856,  spawn_x = 216.257, spawn_y= -787.11, spawn_z= 29.856, activationDist=1.5, markerWidth = 2.0001, markerType= 50, markerColor= 3, spawnable=1},
    {name= 'Garages', x = -738.2580, y= -60.6600, z= 40.7532, spawn_x = -743.191, spawn_y= -63.3786, spawn_z= 40.7526, activationDist=1.5, markerWidth = 2.0001, markerType= 50, markerColor= 3, spawnable= 1},
    {name= 'Garages', x = 894.326, y= -66.2807, z= 78.4803, spawn_x = 894.326, spawn_y= -66.2807, spawn_z= 78.4803, activationDist= 1.5, markerWidth = 2.0001, markerType= 50, markerColor= 3, spawnable= 1},
    {name= 'Garages', x = 149.697280883789, y= 6581.0478515625, z= 30.8512401580811, spawn_x = 143.836929321289, spawn_y= 6574.91796875, spawn_z= 30.9101867675781, activationDist= 1.5, markerWidth = 2.0001, markerType= 50, markerColor= 3, spawnable= 1},
    {name= 'Garages', x = 1950.88012695313, y= 3773.8466796875, z= 31.1720504760742, spawn_x = 1955.75329589844, spawn_y= 3770.85205078125, spawn_z=  31.2003135681152, activationDist= 1.5, markerWidth = 2.0001, markerType= 50, markerColor= 3, spawnable= 1},
    {name= 'Garages', x = -778.530639648438, y= -2378.4384765625, z= 13.5707263946533, spawn_x = -781.188659667969, spawn_y= -2384.53588867188, spawn_z= 13.570725440979, activationDist= 1.5, markerWidth = 2.0001, markerType= 50, markerColor= 3, spawnable= 1},

    {name= 'Marina', x = -892.246, y= -1382.75, z= 0.63417, spawn_x = -894.890625, spawn_y= -1378.25573730469, spawn_z=  0.634293559789658, activationDist= 10.5, markerWidth = 2.0001, markerType= 356, markerColor= 3, spawnable= 2},
    {name= 'Marina', x = -830.409606933594, y= -1411.26635742188, z= 0.60538136959076, spawn_x = -830.326843261719, spawn_y= -1417.15441894531, spawn_z=  0.475613296031952, activationDist= 10.5, markerWidth = 2.0001, markerType= 356, markerColor= 3, spawnable= 2},
    {name= 'Marina', x = -1800.93188476563, y= -1227.26208496094, z= 0.60127639770508, spawn_x = -1795.86682128906, spawn_y= -1232.21398925781, spawn_z= 0.589679062366486, activationDist= 10.5, markerWidth = 2.0001, markerType= 356, markerColor= 3, spawnable= 2},
    {name= 'Marina', x = -1605.63903808594, y= 5258.59716796875, z= 1.08573627471924, spawn_x =-1602.40356445313, spawn_y= 5259.01123046875, spawn_z= 0.474358767271042, activationDist= 10.5, markerWidth = 2.0001, markerType= 356, markerColor= 3, spawnable= 2},
    {name= 'Marina', x = 3373.74267578125, y= 5183.6533203125, z= 0.46024036407471, spawn_x = 3378.72412109375, spawn_y= 5189.5634765625, spawn_z= 0.391142189502716, activationDist= 10.5, markerWidth = 2.0001, markerType= 356, markerColor= 3, spawnable= 2},
    {name= 'Marina', x = 2842.19140625, y= -720.936340332031, z= 0.00004684925079, spawn_x = 2870.90258789063, spawn_y= -724.305786132813, spawn_z= 0.522374451160431, activationDist= 10.5, markerWidth = 2.0001, markerType= 356, markerColor= 3, spawnable= 2},
    {name= 'Marina', x = 1333.29, y= 4269.36, z= 31.5031, spawn_x = 1333.74, spawn_y= 4263.69, spawn_z= 29.8312, activationDist= 10.5, markerWidth = 2.0001, markerType= 356, markerColor= 3, spawnable= 2},

    {name= 'Heliport', x = 1767.22290039063, y= 3252.03662109375, z= 40.6823348999023, spawn_x = 1770.21508789063, spawn_y= 3239.89013671875, spawn_z= 41.1217155456543, activationDist= 1.5, markerWidth = 2.0001, markerType= 360, markerColor= 3, spawnable= 3},
    {name= 'Heliport', x = -718.352783203125, y= -1435.4462890625, z= 4.00052547454834, spawn_x = -724.53662109375, spawn_y= -1443.95397949219, spawn_z=  4.00051927566528, activationDist= 1.5, markerWidth = 2.0001, markerType= 360, markerColor= 3, spawnable= 3},

}

local lastvhl = 0

local currentUiState = false
local currentspawnzone = {
    x=0,
    y=0,
    z=0
}

AddEventHandler('onClientMapStart', function()
    TriggerServerEvent('garages:storeallvehicles')
end)

RegisterNetEvent('garages:addvhltothelist')
AddEventHandler('garages:addvhltothelist', function(vhl)
    local toadd = { key= vhl.name, model=vhl.model, state= vhl.state, plate= vhl.plate }
    SendNUIMessage({addlist = json.encode(toadd)})
end)

function openGui(spawnable)
    SetNuiFocus(true)
    SendNUIMessage({ openUI = true })
    SendNUIMessage({ spawnable = json.encode({spawnable = spawnable}) })
end
function closeGui()
    SetNuiFocus(false)
    SendNUIMessage({ closeUI = true })
    currentUiState = false
end

-------------------------------------------------- NUI LISTENER --------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
RegisterNUICallback('close', function(data, cb)
    closeGui()
    cb('ok')
end)

RegisterNetEvent('veh_shop:closeGui')
AddEventHandler('veh_shop:closeGui', function()
    closeGui()
end)

RegisterNUICallback('fourriere', function(data, cb)
    TriggerServerEvent('garages:settofourriere', {model= data.model, plate= data.plate})
    cb('ok')
end)

RegisterNUICallback('init', function(data, cb)
    TriggerServerEvent('garages:getplayervehicle', data.spawnable)

    cb('ok')
end)

function checkIfZoneIsClear()
    local car = GetClosestVehicle(currentspawnzone.x,currentspawnzone.y,currentspawnzone.z, 5.000, 0, 70)
    local heli = GetClosestVehicle(currentspawnzone.x,currentspawnzone.y,currentspawnzone.z, 5.000, 0, 16384)
    local boat = GetClosestVehicle(currentspawnzone.x,currentspawnzone.y,currentspawnzone.z, 5.000, 0, 12294)
    local plane = GetClosestVehicle(currentspawnzone.x,currentspawnzone.y,currentspawnzone.z, 15.000, 0, 16386)
    if DoesEntityExist(car) or DoesEntityExist(heli) or DoesEntityExist(boat) or DoesEntityExist(plane)then
        return false
    else
        return true
    end
end

RegisterNUICallback('spawn', function(data, cb)
    if not checkIfZoneIsClear() then
        drawNotification("La zone est ~r~encombrée~r~")
        closeGui()
    else
        if data.state == 0 then
            TriggerServerEvent('garages:getvehicle', {model = data.model, plate = data.plate })

        elseif data.state == 1 then
            drawNotification("Votre véhicule est déjà sortis")
            closeGui()
        else
            drawNotification("Votre véhicule est à la fourrière")
        end
    end
    cb('ok')
end)

RegisterNetEvent('garages:store')
AddEventHandler('garages:store', function()
    local current = GetPlayersLastVehicle(GetPlayerPed(-1), true)
    lastvhl = current
    TaskLeaveVehicle(GetPlayerPed(-1), current, 4160)
    Wait(1500)
    if current == 0 then
        drawNotification("Aucun véhicule dans la zone !")
    else
        local model = GetEntityModel(current)
        local plate = GetVehicleNumberPlateText(current)
        TriggerServerEvent('garages:storevehicule', {model = tostring(model), plate = plate })
    end
end)

RegisterNUICallback('store', function(data, cb)
    TriggerEvent('garages:store')
    cb('ok')
end)

RegisterNetEvent('garages:vehiculestored')
AddEventHandler('garages:vehiculestored', function(vhl)
    local current =  GetPlayersLastVehicle(GetPlayerPed(-1), true)
    SetEntityAsMissionEntity(current, true, true)
    Wait(300)
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(current))
    drawNotification("Véhicule: ~g~rentré~g~")
    closeGui()
end)

RegisterNetEvent('garages:notifs')
AddEventHandler('garages:notifs', function(notif)
    drawNotification(notif)
end)

RegisterNetEvent('garages:close')
AddEventHandler('garages:close', function()
    closeGui()
end)

RegisterNetEvent('garages:spawnvehicle')
AddEventHandler('garages:spawnvehicle', function(vhl)
	closeGui()
	local customs = json.decode(vhl.customs)
    Wait(300)
    if checkIfZoneIsClear() then
        local car = tonumber(vhl.model)
        RequestModel(car)
        while not HasModelLoaded(car) do
            Citizen.Wait(0)
        end
        local veh = CreateVehicle(car, currentspawnzone.x,currentspawnzone.y,currentspawnzone.z, 0.0, true, false)
        SetVehicleNumberPlateText(veh, vhl.plate)
        SetVehicleOnGroundProperly(veh)
        SetVehicleHasBeenOwnedByPlayer(veh,true)
        SetEntityAsMissionEntity(veh, true, true)
        local id = NetworkGetNetworkIdFromEntity(veh)
        SetNetworkIdCanMigrate(id, true)
        SetEntityInvincible(veh, false)
        -- Set mod kit to 0 allow modification
        SetVehicleModKit(veh, 0 )
        -- Set color Primary
        SetVehicleModColor_1(veh, customs.color.primary.type, 0,0)
        SetVehicleCustomPrimaryColour(veh, customs.color.primary.red,  customs.color.primary.green,  customs.color.primary.blue)
        -- Set color Secondary
        SetVehicleModColor_2(veh, customs.color.secondary.type, 0,0)
        SetVehicleCustomSecondaryColour(veh, customs.color.secondary.red,  customs.color.secondary.green,  customs.color.secondary.blue)
        -- Set perlescent
        SetVehicleExtraColours(veh, customs.color.pearlescent, customs.wheels.color)
        -- Set mods in db
        for i = 0, 60 do
            ToggleVehicleMod(veh, 18, false)
            SetVehicleMod(veh, i, -1, false)
            if i == 18 and customs["mods"][""..i..""] == 0 then
                ToggleVehicleMod(veh, 18, true)
            end
            if customs.mods[""..i..""] ~= nil then
                SetVehicleMod(veh, i, customs.mods[""..i..""], false)                
            end
        end
        SetVehicleMod(veh, 15, customs.mods["15"], false)
        if vhl.type == 1 then
            -- Set neons
            if customs.neons.enabled then
                ToggleVehicleMod(veh, 22, true)
                SetVehicleNeonLightEnabled(veh, 0, customs.neons.enabled)
                SetVehicleNeonLightEnabled(veh, 1, customs.neons.enabled)
                SetVehicleNeonLightEnabled(veh, 2, customs.neons.enabled)
                SetVehicleNeonLightEnabled(veh, 3, customs.neons.enabled)
                SetVehicleNeonLightsColour(veh, customs.neons.red, customs.neons.green, customs.neons.blue)
            end
            -- Set windows
            SetVehicleWindowTint(veh, customs.windows)
            -- Set Jantes
            SetVehicleWheelType(veh, tonumber(customs.wheels.type))
            SetVehicleMod(veh, 23, tonumber(customs.wheels.choice), false)
            SetVehicleMod(veh, 24, tonumber(customs.wheels.choice), false)
            -- Set Tyreburst
            if  customs.tyreburst.enabled then
                ToggleVehicleMod(veh, 20, true)
                SetVehicleTyreSmokeColor(veh, customs.tyreburst.red, customs.tyreburst.green, customs.tyreburst.blue)
            end
        end
        -- Respray the veh
        SetVehicleDirtLevel(veh)
    else
        drawNotification("Véhicule ~r~dans la zone ~r~")
    end
end)

function DrawMarkers(x,y,z,markerWidth, markerColorRed, markerColorGreen, markerColorBlue, markerAlpha)
    -- drawMarker(type, posX, posY, posZ, dirX, dirY, dirZ, rotX, rotY, rotZ, scaleX, scaleY, scaleZ, colorR, colorG, colorB, alpha, bobUpAndDown, faceCamera, p19, rotate, textureDict, textureName, drawOnEnts);
    DrawMarker(1, x, y, z, 0, 0, 0, 0, 0, 0, markerWidth,  markerWidth,0.101, markerColorRed, markerColorGreen, markerColorBlue,markerAlpha, 0,1,0, 0, 0, 0,0)
end

function setMapMarkers(locations)
    for k,v in ipairs(locations)do
        local blip = AddBlipForCoord(v.x, v.y, v.z)
        SetBlipSprite(blip, v.markerType)
        SetBlipColour(blip, v.markerColor)
        SetBlipScale(blip, 0.8)
        SetBlipAsShortRange(blip, true)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(v.name)
        EndTextCommandSetBlipName(blip)
    end
end

function drawNotification(text)
    SetNotificationTextEntry("STRING")
    AddTextComponentString(text)
    DrawNotification(false, false)
end

Citizen.CreateThread(function()
    setMapMarkers(locations)
    while true do
        Citizen.Wait(0)
        local pos = GetEntityCoords(GetPlayerPed(-1), false)
        -- local hotdog = GetHashKey('prop_weed_01')
        -- local stand = GetClosestObjectOfType(pos.x, pos.y, pos.z, 1.0001, hotdog, false, false, false)
        -- zCitizen.Trace(stand)
        for _,d in ipairs( locations )do
            if Vdist(d.x, d.y, d.z, pos.x, pos.y, pos.z) < 80.0 then
                DrawMarkers(d.x, d.y, d.z, d.markerWidth, 255, 255, 0, 100)
                DrawMarkers(d.spawn_x, d.spawn_y, d.spawn_z, 6.001, 255, 0, 0, 100)
            end
            if(Vdist(d.x, d.y, d.z, pos.x, pos.y, pos.z) < d.activationDist ) then
                SetTextComponentFormat("STRING")
                AddTextComponentString(" ~INPUT_CONTEXT~ pour acceder à votre garage.")
                DisplayHelpTextFromStringLabel(0, 0, 1, -1)
            end
            if(IsControlJustReleased(1, 38) and Vdist(d.x, d.y, d.z, pos.x, pos.y, pos.z) <  d.activationDist ) then
                if currentUiState == false then
                    openGui(d.spawnable)
                    currentspawnzone = {
                        x = d.spawn_x,
                        y = d.spawn_y,
                        z= d.spawn_z,
                    }
                    currentUiState = true
                else
                    closeGui()
                    currentUiState = false
                end
            end
        end

    end
end)
