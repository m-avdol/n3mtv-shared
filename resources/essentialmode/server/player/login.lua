-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --

-- Loading MySQL Class
require "resources/mysql-async/lib/MySQL"

function LoadUser(identifier, asource, new)
	local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = identifier})
	local group = groups[result[1].group]
	Users[asource] = Player(asource, result[1].permission_level, result[1].money, result[1].dirty_money, result[1].identifier , group, result[1].nom, result[1].prenom, result[1].phone_number, result[1].isFirstConnection )
	TriggerEvent('es:playerLoaded', asource, Users[asource])
	TriggerClientEvent('es:setPlayerDecorator', asource, 'rank', Users[asource]:getPermissions())
	TriggerClientEvent('es:setMoneyIcon', asource, settings.defaultSettings.moneyIcon)
	updateLastSeen(identifier)
	if(new)then
		TriggerEvent('es:newPlayerLoaded', asource, Users[asource])
	end
end

function updateLastSeen(identifier)
	MySQL:executeQuery("UPDATE users SET last_seen_at = CURRENT_TIMESTAMP WHERE identifier = '@identifier'",
		{['@identifier'] = identifier})
end

function stringsplit(self, delimiter)
  local a = self:Split(delimiter)
  local t = {}

  for i = 0, #a - 1 do
     table.insert(t, a[i])
  end

  return t
end

function isIdentifierBanned(id)
	local result = MySQL.Sync.fetchAll("SELECT * FROM bans WHERE banned = @name", {['@name'] = id})
	if(result)then
		for k,v in ipairs(result)do
			local now = os.time()
			if v.expires > now then
				return true
			end
		end
	end
	return false
end



function isWhiteListed(identifier)
	local result = MySQL.Sync.fetchAll("SELECT * FROM whitelist WHERE identifier = @name", {['@name'] = identifier})
	if(result)then
		for k,v in ipairs(result)do
			if v.listed == 1 or v.listed == 99 then
				return false
			end
		end
	end
	return true
end


AddEventHandler('es:getPlayers', function(cb)
	cb(Users)
end)

function hasAccount(identifier)
	local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = identifier})
	if(result and result[1] ~= nil) then
		return true
	end
	return false
end


function isLoggedIn(source)
	if(Users[GetPlayerName(source)] ~= nil)then
	if(Users[GetPlayerName(source)]['isLoggedIn'] == 1) then
		return true
	else
		return false
	end
	else
		return false
	end
end

function registerUser(identifier, source)
	if not hasAccount(identifier) then
		-- Inserting Default User Account Stats
		MySQL.Sync.execute("INSERT INTO users (`identifier`, `permission_level`, `money`, `group`) VALUES (@username, '0', @money, 'user')",
			{['@username'] = identifier, ['@money'] = settings.defaultSettings.startingCash})
		LoadUser(identifier, source, true)
	else
		LoadUser(identifier, source, false)
	end
end

AddEventHandler("es:setPlayerData", function(user, k, v, cb)
	if(Users[user])then
		if(Users[user][k])then

			if(k ~= "money") then
				Users[user][k] = v

				MySQL.Async.execute("UPDATE users SET " ..k.."=@value WHERE identifier = @identifier",
					{['@value'] = v, ['@identifier'] = Users[user]['identifier']})
			end

			if(k == "group")then
				Users[user].group = groups[v]
			end

			cb("Player data edited.", true)
		else
			cb("Column does not exist!", false)
		end
	else
		cb("User could not be found!", false)
	end
end)

AddEventHandler("es:setPlayerDataId", function(user, k, v, cb)
	MySQL.Async.execute("UPDATE users SET " ..k.."=@value WHERE identifier = @identifier",
		{['@value'] = v, ['@identifier'] = user})

	cb("Player data edited.", true)
end)

AddEventHandler("es:getPlayerFromId", function(user, cb)
	if(Users)then
		if(Users[user])then
			cb(Users[user])
		else
			cb(nil)
		end
	else
		cb(nil)
	end
end)

AddEventHandler("es:getAllPlayerConnected", function(cb)
	return cb(Users)
end)

AddEventHandler("es:getPlayerFromIdentifier", function(identifier, cb)
	local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = identifier})
	if(result and result[1])then
		cb(result[1])
	else
		cb(nil)
	end
end)

AddEventHandler("es:getAllPlayers", function(cb)
	local result = MySQL.Sync.fetchAll("SELECT * FROM users", {})
	if(result)then
		cb(result)
	else
		cb(nil)
	end
end)

-- Function to update player money every 60 seconds.
local function savePlayerMoney()
	SetTimeout(60000, function()
		TriggerEvent("es:getPlayers", function(users)
			for k,v in pairs(users) do
				MySQL.Async.execute("UPDATE users SET money=@value, dirty_money=@v2 WHERE identifier = @identifier",
					{['@value'] = v.money, ['@v2'] = v.dirty_money, ['@identifier'] = v.identifier})
			end
		end)
		savePlayerMoney()
	end)
end

savePlayerMoney()
