
livreur_pay = {
	id=21,
	x=-328.96,
	y=-2619.24,
	z=6.00,
	minimum = 500,
	maximum = 600
}

livreur_garage = {
	["Livreur"] = {
		model='flatbed',
		x=-409.75,
		y=-2795.61,
		z=5.00,
		radius=-45.00
	},
	["Chargeur"] = {
		model='forklift',
		-- x=-409.90,
		-- y=-2750.46,
		-- z=5.00,
		-- radius=0.00
		x=-403.43,
		y=-2681.72,
		z=5.00,
		radius=-90.00
	}
}

livreur_blips = {
	["Entreprise"] = {
		id=17,
		x=-422.12,
		y=-2787.52,
		z=5.00,
		distanceBetweenCoords=5,
		distanceMarker=7,
		defaultTime=5000
	},
	["Garage1"] = {
		id=18,
		x=-406.44,
		y=-2804.31,
		z=5.00,
		distanceBetweenCoords=5,
		distanceMarker=7,
		defaultTime=5000
	},
	["Remorque"] = {
		id=19,
		x=-403.31,
		y=-2708.61,
		z=5.00,		
		distanceBetweenCoords=6,
		distanceMarker=8,
		defaultTime=5000
	},
	["Commande"] = {
		id=20,
		x=-413.39,
		y=-2697.21,
		z=5.00,
		distanceBetweenCoords=3,
		distanceMarker=6,
		defaultTime=5000
	},
	["Garage2"] = {
		id=21,
		x=-399.67,
		y=-2687.86,
		z=5.00,
		-- x=-417.74,
		-- y=-2740.01,
		-- z=5.00,
		distanceBetweenCoords=5,
		distanceMarker=7,
		defaultTime=5000
	},
	["Contener"] = {
		id=22,
		x=-378.84,
		y=-2672.63,
		z=5.09,
		-- x=-345.28,
		-- y=-2634.61,
		-- z=5.09,
		-- x=-392.03,
		-- y=-2679.72,
		-- z=5.09,
		distanceBetweenCoords=10,
		distanceMarker=13,
		defaultTime=5000
	},
	["depot"] = {
		id=23,
		x=1738.72,
		y=3283.57,
		z=39.90,
		-- x=-328.96,
		-- y=-2619.24,
		-- z=6.00,
		distanceBetweenCoords=7,
		distanceMarker=7,
		defaultTime=5000
	}
}

